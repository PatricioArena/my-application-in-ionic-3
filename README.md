-------------------------------------------------------------------------------
Push Notification con FireBase.

- Para configurar la recepcion de las notificacines de debe reemplazar el 
google-services.json y <'variable name="SENDER_ID" value="ID del remitente" /> en 
config.xml, antes de la compilacion.

- Para generar dicho archivo luego de logear en nuestra cuenta de firebase y 
seleccionar el proyecto se debe dirigir a:

- Configuracion -> General -> AgregarApp -> Seleccionar en este caso Android.

- 1) Registrar el nombre del paquete io.ionic.starter 
- 2) Descargar el JSON. Y reemplazar 
- 3) Siguiente
- 4) Siguiente
- 5) Omitir este paso

- Para obtener ID del remitente se debe dirigir a:

- Configuracion -> Cloud Messaging "Credenciales del proyecto"

Compilar, probar en emulador o dispositivo con android 7 +

- Las notificaciones de pueden enviar desde el proyecto Firebase.

- Crece -> Cloud Messaging -> Send your first message, Configurar los campos a 
eleccion en "Orientar al usuario / App" seleccionar io.ionic.starter por ultimo 
Publicar -> Publicar.

Si todo sale bien deberia estar recibiendo la notificacion en el emulador o 
dispositivo seleccionado.

-------------------------------------------------------------------------------